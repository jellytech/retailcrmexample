#
# Business
#
# Validation tools
#

#'
#' The same as stopifnot but with message.
#'
#' @param cond Condition to be evaluated
#' @param fail_msg Message for stop if condition is FALSE. If not passed
#'    \code{cond} code itself is used for the message.
#'
#' @keywords internal
#' @noRd
#'
assert <- function(cond, fail_msg = NULL, ...) {
  if (!cond) {
    if (is.null(fail_msg) || missing(fail_msg)) {
      fail_msg <- sprintf("Condition failed: %s", deparse(substitute(cond), width.cutoff = 30L))
    } else {
      fail_msg <- sprintf(fail_msg, ...)
    }
    stop(fail_msg, call. = FALSE)
  }
  invisible()
}
