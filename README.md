# RetailCRM

Load data using Manager:

```
recipes.py fs_load -i retailcrm-example -u https://storage.googleapis.com/mlplatform-data/retailcrm-example.zip
```

# TODO
* proper handling of customers without puchases
* proper handling of new/unknown product categories (factorization from FeatureTransfors/FeatureStore?)
* lighter development of FS meatadata: define them, test them, sync only then you are ready
